const readline = require('readline')
const fs = require('fs')
const moment = require('moment')

const readInterface = readline.createInterface({
    input: fs.createReadStream('data.txt'),
    console: false
});

// constants
const BATTERY = 'BATT'
const TSTAT = 'TSTAT'
const DURATION = moment.duration(5, 'minutes')

//globals
const pendingAlerts = {}
const verifiedAlerts = []

// check low limits
const checkLow = (yellowLowLimit, redLowLimit, rawValue) => {
    return rawValue < redLowLimit ? 'RED LOW' :
        rawValue < yellowLowLimit ? 'YELLOW LOW' :
            'NORMAL'
}

// check high limits
const checkHigh = (yellowHighLimit, redHighLimit, rawValue) => {
    return rawValue > redHighLimit ? 'RED HIGH' :
        rawValue > yellowHighLimit ? 'YELLOW HIGH' :
            'NORMAL'
}

const createAlert = (satelliteId, severity, component, timestamp) => (
    {satelliteId, severity, component, timestamp}
)

// business logic
const updateAlert = (satelliteId, severity, component, timestamp) => {
    const date = moment(timestamp, 'YYYYMMDD hh:mm:ss.SSS').utc(true).toISOString()
    const alert = createAlert(satelliteId, severity, component, date)
   // console.log('created alert:', alert)
    if ( !pendingAlerts[satelliteId]){
        pendingAlerts[satelliteId] = {}
    }
    if (!pendingAlerts[satelliteId][component]) {
        pendingAlerts[satelliteId][component] = []
    }
    if(pendingAlerts[satelliteId][component].length === 0){
        pendingAlerts[satelliteId][component] = [alert]
    } else {
        //check if its been less than DURATION minutes from last alert
        const expireTime = new moment(pendingAlerts[satelliteId][component][0].timestamp).add(DURATION)
      //  console.log("date:", moment(date))
      //  console.log("expir:", expireTime)
        if(moment(date).isBefore(moment(expireTime))) {
           // console.log('isBefore')
            if (pendingAlerts[satelliteId][component].length == 2){
                verifiedAlerts.push(pendingAlerts[satelliteId][component][0])
                 pendingAlerts[satelliteId][component] = []
            } else {
                pendingAlerts[satelliteId][component].push(alert)
            }
        } else {
             // console.log('isAfter')
             pendingAlerts[satelliteId][component] = []
        }
    }
}

// process all lines in the file
readInterface.on('line', function(line) {
    // parse line
    const [
        timestamp,
        satelliteId,
        redHighLimit,
        yellowHighLimit,
        yellowLowLimit,
        redLowLimit,
        rawValue,
        component
    ] = line.split('|');

    let limit
    // each component can have different limits
    switch (component) {
        case BATTERY:
            limit = checkLow(yellowLowLimit, redLowLimit, rawValue-0.0)
            break;
        case TSTAT:
            limit = checkHigh(yellowHighLimit, redHighLimit, rawValue-0.0)
            break;
        default: console.log("Invalid component: " + component)
    }

    if (limit !== 'NORMAL') {
        const date = moment(timestamp, 'YYYYMMDD hh:mm:ss.SSS').toDate()
        updateAlert(satelliteId, limit, component, date)
    }
});

// when done processing output current verified alerts
readInterface.on('close', ()=>{
    console.log(verifiedAlerts)
})
